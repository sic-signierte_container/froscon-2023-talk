THEME := dracula
TRANSITION := slide

build:
	@# ----------------------------------------------------------
	@# Pandoc Hints
	@#   --self-contained	Only works with HTML output, implies --standalone
	@#   --slide-level 2	Heading Level 2 creates new slides
	@#   --css ./theme.css  Contains RL specific theme manipulation
	@# ----------------------------------------------------------
	pandoc \
		--from markdown+grid_tables \
		--to revealjs \
		--output index.html \
		--self-contained \
		--slide-level 2 \
		--css ./theme.css \
		--variable theme="${THEME}" \
		--variable transition="${TRANSITION}" \
		--variable "height: '90%'" \
		--variable "width: '90%'" \
		--variable "slideNumber: 'c/t'" \
		--citeproc \
		--biblio=./citations.bib \
		--csl=./ieee-with-note.csl \
		./slides.md

clean:
	rm -f index.html

.PHONY: build clean
