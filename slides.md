---
title: "The New Old: Supply Chain Security"
subtitle: (with Sigstore and Kubernetes this time)
author: delet0r
date: 2023-08-06
---
# Intro

## Intro

- My name is `delet0r`
- Working in IT-Security by day
- Working on the Prototype Fund Project<br/>SiC (Signed Containers) by night
 
::: notes 
- here we try to ease the usage of container signatures

- So lets quickly go over the agenda of this talk
:::
## Agenda

1. Packaging as Complexity Reduction
2. An Approach to Trust
3. Sigstore
4. Conclusion

::: notes
- Package management as an approach to complexity
  - were I quickly go over the evolution of packaging and what they do to reduce complexity
- Then we'll talk about trust
  - What it is
  - How it relates to security
- Then we finally talk about sigstore
  - Here I show you have they try to reduce necesary trust through their infrastructur
  - How the signing/verification process works
  - and the different ways to integrate it into kubernetes
::: 
# 1. Packaging as Complexity Reduction

::: notes 

- Packaging as complexity reduction
- or TRANSITION
::: 
## A short and very simplified ~~history~~  tale<br/>of tackling complexity by packaging and<br/>distributing software (aka Supply Chains)

::: notes
- Who of you thinks that Software Packaging is safe, secure, and just complex enough?
- We are fucked right?
- TRANSITION 
:::
## The Software Crisis

![**Figure**: This is fine @gunshowcomic:this-is-fine](./attachments/this_is_fine.png){width=40%}

::: notes
- but did you now there were people 50 years ago, that also thought that they are doomed and we just learned to live with it?
- TRANSITION
:::
## The Software Crisis

- Term coined in 1968 at the [NATO Software Engineering Conference](https://en.wikipedia.org/wiki/Software_crisis)
- Referenced by Edsger Dijkstra in his Book [The Humble Programmer](http://www.cs.utexas.edu/users/EWD/ewd03xx/EWD340.PDF)
- Describes the problems arising with the increasing complexity of programs

::: notes 
- Term was create because the increasing power of computers quickly led to increasing program size & complexity and therefore to declining quality:
- resulted in:
  - unmaintainable programs
  - many moving parts
  - projects running over-time
  - projects running over-budget
  - inefficient programs
- We've seen that overwhelming complexity basically exists for as long as their is programming 
- We just found ways to reduce complexity just enough for it to work
- So now lets have a look on how package management is actually a try to do exactly that
- reduce complexity
:::

## Tackling Complexity

- Packaging programs is one way to reduce complexity
- Hiding intricacies of a piece of software
  - Build system
  - Transitive dependencies
- Put burden on the developer/distributor

::: notes
- There was a time were we build and deployed all our programs ourselfes
- This did cost a lot of time and effort (as they have realised in 1968 as well)
- One way we tried tackling complexities was by packaging up our programs
- So the developers or the distributors of a piece of software can put the program in a format, that you can distribute to any machine you like
- This is great less work for me, more work for them

- From this humble beginning packaging developed to what it is today, which happend in essentially 3 stages TRANSITION
:::


## The ~~Escalation~~ Evolution of Packaging{data-transition="none"}

1. **Distribution**: Package Management
2. **Deployment**: Containerization
3. **Operation**: Cloud/Container Orchestration

## The Evolution of Packaging{data-transition="none"}

1. **Distribution**: Package Management
2. Deployment: Containerization
3. OperationCloud/Container: Orchestration

::: notes
- Package Management
- So, essentially package management was the answer to the question: How do we package and distribute software between unknown parties
- To some extent this worked
- There was a domain specific central instances maintained by a small set of people
- with some crypto ensuring the distirbuted packages weren't tempered with and actually from the package manager 
- you may know some of their implementations, like
  - apt
  - pacman
  - pkg
  - and the likes
:::

## The Evolution of Packaging{data-transition="none"}

1. Distribution: Package Management
2. **Deployment**: Containerization
3. Operation: Cloud/Container Orchestration

::: notes
- Then Containers came around and made packaging more composable
- you know stuff like docker or podman and the likes
- from one moment to the other we were suddenly able to really compose software
- just take the image and build your software on top of it
- This was possible by making deployment more opinionated, splitting the package into:
  - Actual software
  - State
  - Configuration
- Defining interfaces that standardized those components:
  - Container Runtime
  - Container Storage Interface
  - Container Network Interface
- Creating infrastructure around distributing packages (registries)
  - so everybody can distribute packages
- And then we went even further
:::

## The Evolution of Packaging{data-transition="none"}

1. Distribution: Package Management
2. Deployment: Containerization
3. **Operation**: Cloud/Container Orchestration

::: notes
- So Orchestration quickly followed
- Basically, we took what we did with Containers and used the approach package infrastructure configuration:
  - Helm 
  - Terraform
  - and similar solutions
- So yeah even more complexity
- What you may have realised is, that over time we essentially shifted complexity away from the user to the developer/distributor
- This in turn resulted in even more complexity due to the need to actually generalize the solutions
- We can discuss until kingdom come if this is actually good or not, but there was one thing definitely missed in the modern iterations of packaging
- TRANSITION
::: 

## Shortcomings
- Modern iterations of packaging miss important features of good<br/>package management: 
  1. Integrity
  2. Authenticity

::: notes
- So ok, now everbody is able to package up whole infrastructures and put them into the world
- But we have no widespread method to check if what we receive is hasn't been changed or if it's even from the developer/distributor 
- Now we start to realize, that this is a problem and develop ad-hoc fixes for this
- We could go with what package manager do and what they implemented a long time ago
- Maintainers were trusted to properly handle packaging, updates, and distribution
- PKIs were used sign the distributed packages proving that they are integer and authentic
- Unfortunately due to the increase in copmplexity we can't just use this approach
- This is mainly due to the reason that this system was developed for a few instances were we trust the developers
- This doesnt scale in a world were everybody can run a registry
:::

## Where are we?

- Lower complexity bought with increased complexity somewhere else 
- Any package has potential hidden tail of dependencies
- No practical way of ensuring their authenticity or integrity <br/>(most of the time)
- No conscious entity (like a team maintaining a Distro)we can<br/>trust that does this for us

::: notes
- We bought us complexity reduction on one end but increased it somewhere else
- Packages can come from anywhere and can, due to composabitlity, easily have a hidden tail of possibly infinite dependencies
- but we have no way to ensure authenticity or integrity ourselves
- and we have no conscious entity, like maintainers, that we can trust to do this for us
:::

## Agenda

1. ~~Packaging as Complexity Reduction~~
2. An approach to Trust
3. Sigstore
4. Conclusion

# 2. An Approach to Trust

## What is Trust?

- Defining trust, especially in context of IT, is challenging
- Becomes mushy when mixing technology with human interaction
- Trust is the belief in the integrity of a person or a group

::: notes
- defining trust is hard
- especially when we mixing technology and human interaction
- to differentiate this, I want to explicitly exclude technological system here and want to defin trust as  the belief in the integrity of a person or a group
:::

## Proof is Not Trust

- When setting up a system there are different things can be<br/>done to ensure its security:
  - Cryptography
  - Formal proofs
- This is not trust but rather logical proofs 

::: notes
  - There are different ways to set up a system securely
  - The two relevant today are:
    - Cryptography Can be used to prove the persistence of properties
    - Formal proofs: To show the that program follow expected behavior
  - Of course, there are more but these are very central to the problem at hand
  - Importantely, approaches rely on logical proof rather than trust as we defined it
:::

## Suspension of Disbelief

- In every system there is a point were we can't technically<br/>prove properties anymore
- For that we need Suspension of Disbelief ... or **Trust**

::: notes
- Taking from literary theory: Suspension of Disbelief the willing acceptance of the impossibility in reality
- In every system there is a point were we can't technically prove properties anymore
- Either because we just can't, since it's not something we can verify, e.g. :
  - the adherence to processes (or the appliance of them)
  - people not making mistakes (what happens if the crypto implementation is faulty)
  - people acting without malicious intend
- Or because actually verifying the correctness would just be too expensive
- Here trust in the people and procedures is needed as basis for the security system
- So what we mean here ist the acceptance of unproovablity 
- So with this out of the way we can now talk about two different ways that try to enable you to ensure the intgerity and authenticity of packages
::: 

## Different ways of balancing Trust and Proofs

- **a.** Content Moderation (Trust first)
- **b.** Signatures (Proof first)

::: notes
- Trust vs Proof is a spectrum
- Different systems have a different distribution between trust and proof
- I know want to give two examples than can be considered on opposite sites of this spectrum
- So you can get a feeling for them
:::
## a. Content Moderation

- Trust first approach
- System were a set of people or an institution is trusted<br/>with moderation
- Trust in the decisions of the people/institution necessary

::: notes
- System were a set of people or an institution is trusted with moderation
- Moderation here means that there is a process ment to ensure that users either only get trusted resources or can differntiate them from untrusted ones
- In our case moderation make sure packages are authentic and integer
- And for this system to work we need to trust in the process of moderation itself and in the instution

- as with package management this approach doesn't scale really well when we have a lot of providers
:::
## a. Docker Hub Trusted Content

- Docker Hub Trusted Content
- Released on [May 27th 2021](https://www.docker.com/press-release/docker-expands-trusted-content-offerings/)
- Uses different flags on images
- Flags indicate different reasons why docker deems<br/>them trustworthy

::: notes
- One example for Moderation is the Docker Hub Trusted Content System:
  - Uses different flags on images
  - Flags indicate different reasons why docker deems them trustworthy
- TRANSITION
:::

## a. Docker Hub Trusted Content in Real Life

![**Figure**: Docker Hub Flag Screenshot](./attachments/Pasted_image_20230722152530.png){width=50%}

::: notes
- Here you can see a screenshot from docker hub
- We see the results of a search
- Every you see here has an tag added to it
- Let's zoom in to see what tags exist
:::
## a. Docker Hub Trusted Content in Real Life

![Docker Hub Flag Screenshot](./attachments/docker_flags_zoomed.png){width=50%}

::: notes
- there are 3 types of images
  - docker official image:  images actually published by docker
  - verified publishers: images were docker has verified that the mages comes from a known publisher
  - sponsored oss: open source projects that are sponsored by docker
- So as long as we trust docker in their decisions and proceses this great
:::
## b. Signatures

- Proof first approach
- Signing packages to proof the integrity and authenticity<br/>of an package
- Trust has to be put in the handling of keys and implementation<br/> of processes

::: notes
- With the signature approach we sign a package  and make the signature available for everybody to check
- Everybody with the necessary information can check if the package is authentic and has not been tempered with
- There are no further guarantees, e.g. a secure creation process
- Here the necessary trust is reduced to the handling of keys and processes
:::

## b. Signature

- Wasn't initially possible with [Docker Images](https://www.redhat.com/en/blog/container-image-signing)
- Changed with Docker Image Format 1.10
- Current examples:
  - Docker Trust System
  - Sigstore

::: notes
- Signing images wasn't possible due to the docker image specification not having reliable checksums
- This changed with the introduction of docker image format version 1.10 in 2016

(in case anybody wants to know OCI container layout got specified in 2017)

Transition to Sigstore
:::

## Agenda

1. ~~Packaging as Complexity Reduction~~
2. ~~An Approach to Trust~~
3. Sigstore
4. Conclusion

# 3. Sigstore

## Sigstore

- Collection of tools, standards and processes
- Ensures integrity and authenticity of artifacts independent of registry
- Purpose build for use with Containers and Container Orchestration
- Uses short lived certificates and certificate transparency

::: notes
- Sigstore is a [Linux Foundation project](https://www.linuxfoundation.org/projects)
- Purpose build around ensuring integrity and authenticity in context of container and container orchestration systems
  - Interacts with OCI compliant registries
  - provides tools for integration into container orchestration like Kubernetes and Open Shift
- Uses short lived certificates and certificate transparency to reduce necessary trust 
- For the uninitiated:
- short lived certifcates:
  - usually 10 min valid
  - as defense against possible key compromise
  - as a workaround to problems inherent to key revocation
-  certificate transparency is the process of writing all created certificates into an append-only log that is publically monitored
  - that way even if compromise arises it can be detected
- There they can be monitored for integrity, thereby allowing to detect compromised certificates
 - We chose Sigstore specifically due to these features and the way it appraches trust
:::

## How Sigstore Handles Trust?

- Uses different technologies to reduce necessary trust<br/>through it's components
- Infrastructure consists of:
  - Trust Root
  - Fulcio (+ additional certificate transparency log) 
  - Rekor
  - (Cosign, CLI for signing and verifying)
  
::: notes
- Specifically, Sigstore created an infrastructure with different components that aim to reduce the necessary trust
- Here we will use the sigstore implementation of the infrastucture as reference (self-hosted solution with variations in components are possible)
- The components are
  - Trust Root
  - Fulcio (+ additional certificate transparency log) 
  - Rekor
  - Cosign
:::

## Sigstores Infrastructure Overview

![**Figure**: Sigstore Infrastructure Overview @Sigstore:SigningFlow](./attachments/sigstore_landscape.drawio.svg)

::: notes
- Overview over the infrastructure
- can see all components and actions usually taken
:::

## Sigstore Infrastructure Components{data-transition="none"}

![**Figure**: Sigstore Infrastructure Components](./attachments/sigstore_landscape_infrastructure_components.drawio.svg)



## Sigstore TUF{data-transition="none"}

![**Figure**: Sigstore Infrastructure Components](./attachments/sigstore_landscape_infrastructure_components_tuf.drawio.svg)

::: notes
- so lets start with the infrastructure
- first we have the trust root
  - it holds and secures all root keys and provides public keys and certificate that are necessary to validate signatures
  - All infrastructure keys are derived from here
  - That's why it's the base of Trust to the whole project
  - This property is then extended to all other components via signatures/certificates
  - To further reduce trust necessary in the root Sigstore uses the tuf protocol:
     - allow to define key specific rules policies (like key updates)
     - provides a regression model in case keys are lost or compromised
     - protects against different attacks
     - provides delegation (key hirarchy)
:::
## Sigstore Fulcio{data-transition="none"}

![**Figure**: Sigstore Infrastructure Components](./attachments/sigstore_landscape_infrastructure_components_fulcio.drawio.svg)


::: notes
  - Fulcio
    - Signing Certificate CA
    - Issues cert on OpenID connect identity (the oidc is external and considered trustworthy)
    - Sigstore supports different OIDC formats and has a list of supported providers
    - All created certificates are appended to the transparency log for monitoring
    - Trasparency logs also reduce trust through the neet fact of transparency
    - I don't have to trust in the implementation of a process when I can monitor it through an append only log 
    - it also shifts the burden of checking to the users therby moving necessary trust away from the sigstore ;)
:::
## Sigstore Rekor{data-transition="none"}

![**Figure**: Sigstore Infrastructure Components](./attachments/sigstore_landscape_infrastructure_components_rekor.drawio.svg)

::: notes
  - Rekor
   - Transparency log for signatures
   - timestamps signatures
   - enables check if signature was created when cert was still valid
:::
## Sigstore Signing Process

![**Figure**: Sigstore Signature Process](./attachments/sigstore_landscape_sign.drawio.svg)

::: notes
- Now we are putting it all together and will go through one whole signing/verification [flow](https://docs.sigstore.dev/fulcio/certificate-issuing-overview/)
- caveat:  - [keyless](https://docs.sigstore.dev/cosign/openid_signing/) setup with the default flow only
- Developer obtaines identity token through the oidc provider
- creates a key pair (only for signature not for identity)
- encrypts the token as a challenge to prove the client has the private key (signed oidc token)
- send request with challenge and public key to Fulcio
- Fulcio verifies the given data (challenge and oidc token)
- When sccessfull creates cert bound to given oidc identity  
- Appends cert to ct log with poison extension added
- Log returns proof of inclusion wich is added to cert
- Fulcio Returns cert to client
- Client creates signing event, contains:
  - hash of artifact
  - public key/certificate
  - signature
:::
## Sigstore Publishing Process

![**Figure**: Sigstore Publishing Process](./attachments/sigstore_landscape_publish.drawio.svg)

::: notes

- signing is witnessed by rekor and data uploaded to registry
- after signing the private key is deleted
:::

## Sigstore Verification Process

![**Figure**: Sigstore Verification Process](./attachments/sigstore_landscape_verify.drawio.svg)


::: notes
- Cli gets rekor pk and fulcio root ca from tuf (usually done automatically) which are used for verification
- compare signature of registry with rekor
- matching confirms validity (no private key needed -> fulcio cert checked existence of private key)
- Compare the timestamped cert validity time and signature timestamp 
- Verification is only valid when rekor is monitored by independent parties and found to be integer
:::


## Integrating Sigstore into Kubernetes

- Two ways of integrating Sigstore:
  - **a.** Via the Container Runtime Cri-O
  - **b.** Via an Admission Controller 
  
::: notes 
- We can integrate Sigstore into kubernetes in two different ways
:::

## a. Cri-O Integration

- Integration via [Cri-O](https://cri-o.io/) container runtime
- Alternative OCI (Open Container Initiative)-compatible runtime
- Uses `containers/image` [libraries](https://github.com/containers/)

::: notes
- Cri-O is an alternative runtime developed by the Open Container Initiative
- Currently only Runtime that is able to verify sigstore
- due to the use of `containers/image` libraries for image handling
- `containers` in turn is a community-driven project that develops tools and libraries for the container ecosystem
:::

## a. Policies

- `containers/image` allows to define requirements for downloading images
- [Configured](https://github.com/containers/image/blob/main/docs/containers-policy.json.5.md) via  `policy.json`   file on orchestrator node
- Can require:
  - Scope 
  - Signatures and their formats

::: notes
- Actual integration is via policies
- can be used to define requirements on the download of images
- policies are defined in `policy.json` that are pat of the runtime configuration
- therfor configuration is on a per node basis
:::
  
## a. Cri-O Configuration

- Example configuration for requiring Sigstore signing @policyJson:Example:

![](attachments/policy_json_example.png){style='padding: 0px; box-shadow: none !important'}

::: notes
- "transports" defines modus used to aquire the image
- In this case docker which means the given registry follows the docker registry specification
- then we give the registry path
- in this case not a namespace (scoping) but the explicit image
- now we come to the actual policy
- to allow downloading the image, the needs to have a valid Sigstore Signature
- then we define:
    - necessary public keys for recor and fulcio 
    - the oidc issuer were the runtime can check the validty of the identity token
    - and the subject of the user that has signedthe image
:::

## b. Admission Controller Integration

- Define custom plugins that intercept calls to the Kubernetes API<br/>and either validates or modifies them (since Kubernetes 1.25)
- Sigstore project provides a policy controller to<br/>define requirements on the download of container images
- Is still under development

::: notes
- What is an admission Controller
- How do they usually work
- How to install Sigstore admission Controller
- How to define restrictions
  - Namespaces/Labels

- 
:::

## b. Admission Controller Configuration

- Introduces the `ClusterImagePolicy` object to define polices
- Policies are defined on a per namespace basis
- Enabled by adding label `policy.sigstore.dev/include=true` to a namespace

##  b. Configuring a ClusterImagePolicy

- Example `ClusterImagePolicy` @clusterImagePolicy:Example:

![](attachments/clusterImagePolicy_example.png){width=70% style='padding: 0px; box-shadow: none !important'}

::: notes
- This is an example `ClusterImapePolicy` confiugration
- First we have glob that defines the images this policy applies to
- There can be multiple policies applied to an image
- if that is the case each policy has to have at least one passing authority
- with authorities we can define the signature properties the image has to adhere to
- in our example we define the following:
  - the image has to have a keyless signature
    - the other two supported types are static and self-managed-keys
  - Certificates are to be checked against the given Fulcio instance which should have CA matching the given public key
  - The identities used to sign the image are either by a OIDC token of the specified email
  - or a github workflow oidc token that matches a repository glob
- There are a lot more thing we can specify but most of them allow to use self-hosted sigstore infrastructure or specifics of the other authorities

:::


## Agenda

1. ~~Packaging as Complexity Reduction~~
2. ~~An Approach to Trust~~
3. ~~Sigstore~~
4. Conclusion

# 4. Conclusion

## Conclusion

  - Provides system that ensures authenticity and integrity
  - Registry independent
  - Works as independent third party
  - Makes Package the Management approach scalable

::: notes
- We have seen how the sigstore system allows to ensure authenticity and integrity for packges
- It does that while being independent of registries by being a third party
- So what essentially has happend is, that sigstore took the approach of package management 
- and applied it to the complexities of modern packaging by being e third party over wich integrity and authenticity of a package can be verified independently
:::
## Caveats


- Solves a very specific problem
- Good security practices and transparent processes have to follow
- Introduces complexity as well
- In the end a corner stone to build upon

::: notes
- The problem sigstore solves is a very specific one
- There was a reason I focused on the authenticity and integrity of packages
- This means to have something resembling a secure system there is a lot more needed, like:
  - good coding practices
  - proper and up to date documentation
  - and much more
- Additionally to the things it solves it again adds complexity, that has to be weighted for it's benefits
- So in the end if sigstore solves problems you have it is corner stone to build upon
:::
# Thank you :)

:::: {.column2}
::: {.section1}
- Contact me!
   - Mastodon: [https://chaos.social/\@delet0r](https://chaos.social/\@delet0r)
   - Threema-ID: FW4PW5C8
   - Matrix: \@delet0r:catgirl.cloud
:::
::::
:::: {.column3}
::: {.section2}
<br/>
![Slide Repository](./attachments/repo_qr.svg)
:::
::::
# External Images & Examples

::: {#refs .allowframebreaks}
:::
